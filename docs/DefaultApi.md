# KigaliApi.DefaultApi

All URIs are relative to *https://7rfbtov049.execute-api.us-east-1.amazonaws.com/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**insuranceGet**](DefaultApi.md#insuranceGet) | **GET** /insurance | 
[**insuranceInsuranceIdDelete**](DefaultApi.md#insuranceInsuranceIdDelete) | **DELETE** /insurance/{insuranceId} | 
[**insuranceInsuranceIdPharmacyIdDelete**](DefaultApi.md#insuranceInsuranceIdPharmacyIdDelete) | **DELETE** /insurance/{insuranceId}/{pharmacyId} | 
[**insuranceInsuranceIdPharmacyIdPost**](DefaultApi.md#insuranceInsuranceIdPharmacyIdPost) | **POST** /insurance/{insuranceId}/{pharmacyId} | 
[**insuranceInsuranceIdPut**](DefaultApi.md#insuranceInsuranceIdPut) | **PUT** /insurance/{insuranceId} | 
[**insurancePost**](DefaultApi.md#insurancePost) | **POST** /insurance | 
[**inventoryInventoryItemIdPut**](DefaultApi.md#inventoryInventoryItemIdPut) | **PUT** /inventory/{inventoryItemId} | 
[**inventoryPharmacyIdGet**](DefaultApi.md#inventoryPharmacyIdGet) | **GET** /inventory/{pharmacyId} | 
[**inventoryPharmacyIdProductIdDelete**](DefaultApi.md#inventoryPharmacyIdProductIdDelete) | **DELETE** /inventory/{pharmacyId}/{productId} | 
[**inventoryPharmacyIdProductIdPost**](DefaultApi.md#inventoryPharmacyIdProductIdPost) | **POST** /inventory/{pharmacyId}/{productId} | 
[**locationGet**](DefaultApi.md#locationGet) | **GET** /location | 
[**pharmacyGet**](DefaultApi.md#pharmacyGet) | **GET** /pharmacy | 
[**pharmacyOwnedGet**](DefaultApi.md#pharmacyOwnedGet) | **GET** /pharmacy/owned | 
[**pharmacyPharmacyIdDelete**](DefaultApi.md#pharmacyPharmacyIdDelete) | **DELETE** /pharmacy/{pharmacyId} | 
[**pharmacyPharmacyIdGet**](DefaultApi.md#pharmacyPharmacyIdGet) | **GET** /pharmacy/{pharmacyId} | 
[**pharmacyPharmacyIdMemberGet**](DefaultApi.md#pharmacyPharmacyIdMemberGet) | **GET** /pharmacy/{pharmacyId}/member | 
[**pharmacyPharmacyIdMemberPost**](DefaultApi.md#pharmacyPharmacyIdMemberPost) | **POST** /pharmacy/{pharmacyId}/member | 
[**pharmacyPharmacyIdOpeningTimesGet**](DefaultApi.md#pharmacyPharmacyIdOpeningTimesGet) | **GET** /pharmacy/{pharmacyId}/opening-times | 
[**pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete**](DefaultApi.md#pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete) | **DELETE** /pharmacy/{pharmacyId}/opening-times/{openingTimeId} | 
[**pharmacyPharmacyIdOpeningTimesPost**](DefaultApi.md#pharmacyPharmacyIdOpeningTimesPost) | **POST** /pharmacy/{pharmacyId}/opening-times | 
[**pharmacyPharmacyIdPatch**](DefaultApi.md#pharmacyPharmacyIdPatch) | **PATCH** /pharmacy/{pharmacyId} | 
[**pharmacyPost**](DefaultApi.md#pharmacyPost) | **POST** /pharmacy | 
[**productGet**](DefaultApi.md#productGet) | **GET** /product | 
[**productPost**](DefaultApi.md#productPost) | **POST** /product | 
[**productProductIdDelete**](DefaultApi.md#productProductIdDelete) | **DELETE** /product/{productId} | 
[**productProductIdInventoryItemsGet**](DefaultApi.md#productProductIdInventoryItemsGet) | **GET** /product/{productId}/inventory-items | 
[**productProductIdPharmaciesGet**](DefaultApi.md#productProductIdPharmaciesGet) | **GET** /product/{productId}/pharmacies | 
[**productProductIdPut**](DefaultApi.md#productProductIdPut) | **PUT** /product/{productId} | 



## insuranceGet

> [InsuranceDto] insuranceGet(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'page': 56, // Number | 
  'search': "''" // String | 
};
apiInstance.insuranceGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**|  | [optional] 
 **search** | **String**|  | [optional] [default to &#39;&#39;]

### Return type

[**[InsuranceDto]**](InsuranceDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## insuranceInsuranceIdDelete

> insuranceInsuranceIdDelete(insuranceId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let insuranceId = 789; // Number | 
apiInstance.insuranceInsuranceIdDelete(insuranceId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insuranceId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## insuranceInsuranceIdPharmacyIdDelete

> insuranceInsuranceIdPharmacyIdDelete(insuranceId, pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let insuranceId = 789; // Number | 
let pharmacyId = 789; // Number | 
apiInstance.insuranceInsuranceIdPharmacyIdDelete(insuranceId, pharmacyId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insuranceId** | **Number**|  | 
 **pharmacyId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## insuranceInsuranceIdPharmacyIdPost

> insuranceInsuranceIdPharmacyIdPost(insuranceId, pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let insuranceId = 789; // Number | 
let pharmacyId = 789; // Number | 
apiInstance.insuranceInsuranceIdPharmacyIdPost(insuranceId, pharmacyId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insuranceId** | **Number**|  | 
 **pharmacyId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## insuranceInsuranceIdPut

> insuranceInsuranceIdPut(insuranceId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let insuranceId = 789; // Number | 
let opts = {
  'insuranceDto': new KigaliApi.InsuranceDto() // InsuranceDto | 
};
apiInstance.insuranceInsuranceIdPut(insuranceId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insuranceId** | **Number**|  | 
 **insuranceDto** | [**InsuranceDto**](InsuranceDto.md)|  | [optional] 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## insurancePost

> InsuranceDto insurancePost(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'insuranceDto': new KigaliApi.InsuranceDto() // InsuranceDto | 
};
apiInstance.insurancePost(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insuranceDto** | [**InsuranceDto**](InsuranceDto.md)|  | [optional] 

### Return type

[**InsuranceDto**](InsuranceDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## inventoryInventoryItemIdPut

> inventoryInventoryItemIdPut(inventoryItemId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let inventoryItemId = 789; // Number | 
let opts = {
  'inventoryItemDto': new KigaliApi.InventoryItemDto() // InventoryItemDto | 
};
apiInstance.inventoryInventoryItemIdPut(inventoryItemId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventoryItemId** | **Number**|  | 
 **inventoryItemDto** | [**InventoryItemDto**](InventoryItemDto.md)|  | [optional] 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## inventoryPharmacyIdGet

> [InventoryItemDto] inventoryPharmacyIdGet(pharmacyId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = 789; // Number | 
let opts = {
  'page': 56, // Number | 
  'quantityOrderAsc': true, // Boolean | 
  'search': "''" // String | 
};
apiInstance.inventoryPharmacyIdGet(pharmacyId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **Number**|  | 
 **page** | **Number**|  | [optional] 
 **quantityOrderAsc** | **Boolean**|  | [optional] [default to true]
 **search** | **String**|  | [optional] [default to &#39;&#39;]

### Return type

[**[InventoryItemDto]**](InventoryItemDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## inventoryPharmacyIdProductIdDelete

> inventoryPharmacyIdProductIdDelete(pharmacyId, productId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = 789; // Number | 
let productId = 789; // Number | 
apiInstance.inventoryPharmacyIdProductIdDelete(pharmacyId, productId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **Number**|  | 
 **productId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## inventoryPharmacyIdProductIdPost

> InventoryItemDto inventoryPharmacyIdProductIdPost(pharmacyId, productId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = 789; // Number | 
let productId = 789; // Number | 
apiInstance.inventoryPharmacyIdProductIdPost(pharmacyId, productId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **Number**|  | 
 **productId** | **Number**|  | 

### Return type

[**InventoryItemDto**](InventoryItemDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## locationGet

> SearchResponse locationGet(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'q': "q_example" // String | 
};
apiInstance.locationGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**|  | [optional] 

### Return type

[**SearchResponse**](SearchResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyGet

> [PharmacyDto] pharmacyGet(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'page': 56, // Number | 
  'search': "''", // String | 
  'showNotApproved': true // Boolean | 
};
apiInstance.pharmacyGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**|  | [optional] 
 **search** | **String**|  | [optional] [default to &#39;&#39;]
 **showNotApproved** | **Boolean**|  | [optional] [default to true]

### Return type

[**[PharmacyDto]**](PharmacyDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyOwnedGet

> [PharmacyDto] pharmacyOwnedGet(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'page': 56, // Number | 
  'search': "search_example" // String | 
};
apiInstance.pharmacyOwnedGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**|  | [optional] 
 **search** | **String**|  | [optional] 

### Return type

[**[PharmacyDto]**](PharmacyDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyPharmacyIdDelete

> pharmacyPharmacyIdDelete(pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
apiInstance.pharmacyPharmacyIdDelete(pharmacyId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## pharmacyPharmacyIdGet

> PharmacyDto pharmacyPharmacyIdGet(pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
apiInstance.pharmacyPharmacyIdGet(pharmacyId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 

### Return type

[**PharmacyDto**](PharmacyDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyPharmacyIdMemberGet

> [PharmacyMemberDto] pharmacyPharmacyIdMemberGet(pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
apiInstance.pharmacyPharmacyIdMemberGet(pharmacyId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 

### Return type

[**[PharmacyMemberDto]**](PharmacyMemberDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyPharmacyIdMemberPost

> PharmacyMemberDto pharmacyPharmacyIdMemberPost(pharmacyId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
let opts = {
  'pharmacyMemberDto': new KigaliApi.PharmacyMemberDto() // PharmacyMemberDto | 
};
apiInstance.pharmacyPharmacyIdMemberPost(pharmacyId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 
 **pharmacyMemberDto** | [**PharmacyMemberDto**](PharmacyMemberDto.md)|  | [optional] 

### Return type

[**PharmacyMemberDto**](PharmacyMemberDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## pharmacyPharmacyIdOpeningTimesGet

> [OpeningTimeDto] pharmacyPharmacyIdOpeningTimesGet(pharmacyId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
apiInstance.pharmacyPharmacyIdOpeningTimesGet(pharmacyId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 

### Return type

[**[OpeningTimeDto]**](OpeningTimeDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete

> pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete(pharmacyId, openingTimeId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
let openingTimeId = "openingTimeId_example"; // String | 
apiInstance.pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete(pharmacyId, openingTimeId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 
 **openingTimeId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## pharmacyPharmacyIdOpeningTimesPost

> OpeningTimeDto pharmacyPharmacyIdOpeningTimesPost(pharmacyId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
let opts = {
  'openingTimeDto': new KigaliApi.OpeningTimeDto() // OpeningTimeDto | 
};
apiInstance.pharmacyPharmacyIdOpeningTimesPost(pharmacyId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 
 **openingTimeDto** | [**OpeningTimeDto**](OpeningTimeDto.md)|  | [optional] 

### Return type

[**OpeningTimeDto**](OpeningTimeDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## pharmacyPharmacyIdPatch

> PharmacyDto pharmacyPharmacyIdPatch(pharmacyId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let pharmacyId = "pharmacyId_example"; // String | 
let opts = {
  'pharmacyDto': new KigaliApi.PharmacyDto() // PharmacyDto | 
};
apiInstance.pharmacyPharmacyIdPatch(pharmacyId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyId** | **String**|  | 
 **pharmacyDto** | [**PharmacyDto**](PharmacyDto.md)|  | [optional] 

### Return type

[**PharmacyDto**](PharmacyDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## pharmacyPost

> PharmacyDto pharmacyPost(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'pharmacyDto': new KigaliApi.PharmacyDto() // PharmacyDto | 
};
apiInstance.pharmacyPost(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pharmacyDto** | [**PharmacyDto**](PharmacyDto.md)|  | [optional] 

### Return type

[**PharmacyDto**](PharmacyDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## productGet

> [ProductDto] productGet(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'page': 56, // Number | 
  'pharmacyId': 789, // Number | 
  'search': "search_example" // String | 
};
apiInstance.productGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**|  | [optional] 
 **pharmacyId** | **Number**|  | [optional] 
 **search** | **String**|  | [optional] 

### Return type

[**[ProductDto]**](ProductDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## productPost

> ProductDto productPost(opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let opts = {
  'productDto': new KigaliApi.ProductDto() // ProductDto | 
};
apiInstance.productPost(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productDto** | [**ProductDto**](ProductDto.md)|  | [optional] 

### Return type

[**ProductDto**](ProductDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## productProductIdDelete

> productProductIdDelete(productId)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let productId = 789; // Number | 
apiInstance.productProductIdDelete(productId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## productProductIdInventoryItemsGet

> [InventoryItemDto] productProductIdInventoryItemsGet(productId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let productId = 789; // Number | 
let opts = {
  'sortPrice': false // Boolean | 
};
apiInstance.productProductIdInventoryItemsGet(productId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 
 **sortPrice** | **Boolean**|  | [optional] [default to false]

### Return type

[**[InventoryItemDto]**](InventoryItemDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## productProductIdPharmaciesGet

> [PharmacyDto] productProductIdPharmaciesGet(productId)



### Example

```javascript
import KigaliApi from 'kigali_api';

let apiInstance = new KigaliApi.DefaultApi();
let productId = 789; // Number | 
apiInstance.productProductIdPharmaciesGet(productId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 

### Return type

[**[PharmacyDto]**](PharmacyDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## productProductIdPut

> productProductIdPut(productId, opts)



### Example

```javascript
import KigaliApi from 'kigali_api';
let defaultClient = KigaliApi.ApiClient.instance;
// Configure Bearer (jwt) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new KigaliApi.DefaultApi();
let productId = 789; // Number | 
let opts = {
  'productDto': new KigaliApi.ProductDto() // ProductDto | 
};
apiInstance.productProductIdPut(productId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Number**|  | 
 **productDto** | [**ProductDto**](ProductDto.md)|  | [optional] 

### Return type

null (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

