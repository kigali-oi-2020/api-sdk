# KigaliApi.Poi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**categories** | **[String]** |  | [optional] 


