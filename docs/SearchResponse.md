# KigaliApi.SearchResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**Summary**](Summary.md) |  | [optional] 
**results** | [**[Result]**](Result.md) |  | [optional] 


