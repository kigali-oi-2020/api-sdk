# KigaliApi.PharmacyDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  | [optional] 
**approved** | **Boolean** |  | [optional] 
**id** | **Number** |  | [optional] 
**insurances** | [**[InsuranceDto]**](InsuranceDto.md) |  | [optional] 
**latitude** | **Number** |  | [optional] 
**longitude** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**openingTimes** | [**[OpeningTimeDto]**](OpeningTimeDto.md) |  | [optional] 


