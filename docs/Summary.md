# KigaliApi.Summary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query** | **String** |  | [optional] 
**queryType** | **String** |  | [optional] 
**queryTime** | **Number** |  | [optional] 
**numResults** | **Number** |  | [optional] 
**offset** | **Number** |  | [optional] 
**totalResults** | **Number** |  | [optional] 
**fuzzyLevel** | **Number** |  | [optional] 


