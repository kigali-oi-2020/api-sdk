# KigaliApi.PharmacyMemberDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pharmacyId** | **String** |  | [optional] 
**userId** | **String** |  | [optional] 


