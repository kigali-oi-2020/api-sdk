# KigaliApi.InventoryItemDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**Currency**](Currency.md) |  | [optional] 
**id** | **Number** |  | [optional] 
**pharmacy** | [**PharmacyDto**](PharmacyDto.md) |  | [optional] 
**price** | **Number** |  | [optional] 
**product** | [**ProductDto**](ProductDto.md) |  | [optional] 
**quantity** | **Number** |  | [optional] 


