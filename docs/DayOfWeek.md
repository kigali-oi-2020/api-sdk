# KigaliApi.DayOfWeek

## Enum


* `FRIDAY` (value: `"FRIDAY"`)

* `MONDAY` (value: `"MONDAY"`)

* `SATURDAY` (value: `"SATURDAY"`)

* `SUNDAY` (value: `"SUNDAY"`)

* `THURSDAY` (value: `"THURSDAY"`)

* `TUESDAY` (value: `"TUESDAY"`)

* `WEDNESDAY` (value: `"WEDNESDAY"`)


