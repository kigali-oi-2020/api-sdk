# KigaliApi.EntryPoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**position** | [**Position**](Position.md) |  | [optional] 


