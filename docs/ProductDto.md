# KigaliApi.ProductDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amountOfPharmacies** | **Number** |  | [optional] 
**ean** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**inPharmacy** | **Boolean** |  | [optional] 
**name** | **String** |  | [optional] 
**unit** | **String** |  | [optional] 


