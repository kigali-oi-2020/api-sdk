# KigaliApi.OpeningTimeDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dayOfWeek** | [**DayOfWeek**](DayOfWeek.md) |  | [optional] 
**id** | **String** |  | [optional] 
**pharmacyId** | **String** |  | [optional] 
**startTime** | **String** |  | [optional] 
**stopTime** | **String** |  | [optional] 


