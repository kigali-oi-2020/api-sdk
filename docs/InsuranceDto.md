# KigaliApi.InsuranceDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**insurance** | **String** |  | [optional] 


