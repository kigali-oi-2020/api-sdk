# KigaliApi.Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**streetNumber** | **String** |  | [optional] 
**streetName** | **String** |  | [optional] 
**municipalitySubdivision** | **String** |  | [optional] 
**municipality** | **String** |  | [optional] 
**countrySecondarySubdivision** | **String** |  | [optional] 
**countryTertiarySubdivision** | **String** |  | [optional] 
**countrySubdivision** | **String** |  | [optional] 
**postalCode** | **String** |  | [optional] 
**countryCode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**countryCodeISO3** | **String** |  | [optional] 
**freeformAddress** | **String** |  | [optional] 
**localName** | **String** |  | [optional] 
**extendedPostalCode** | **String** |  | [optional] 


