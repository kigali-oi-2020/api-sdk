# KigaliApi.BtmRightPoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **Number** |  | [optional] 
**lon** | **Number** |  | [optional] 


