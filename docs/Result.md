# KigaliApi.Result

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**score** | **Number** |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**position** | [**Position**](Position.md) |  | [optional] 
**viewport** | [**Viewport**](Viewport.md) |  | [optional] 
**entryPoints** | [**[EntryPoint]**](EntryPoint.md) |  | [optional] 
**poi** | [**Poi**](Poi.md) |  | [optional] 


