# KigaliApi.Viewport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**topLeftPoint** | [**TopLeftPoint**](TopLeftPoint.md) |  | [optional] 
**btmRightPoint** | [**BtmRightPoint**](BtmRightPoint.md) |  | [optional] 


