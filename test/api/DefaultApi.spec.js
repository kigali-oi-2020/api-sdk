/**
 * Kigali API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.KigaliApi);
  }
}(this, function(expect, KigaliApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new KigaliApi.DefaultApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DefaultApi', function() {
    describe('insuranceGet', function() {
      it('should call insuranceGet successfully', function(done) {
        //uncomment below and update the code to test insuranceGet
        //instance.insuranceGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('insuranceInsuranceIdDelete', function() {
      it('should call insuranceInsuranceIdDelete successfully', function(done) {
        //uncomment below and update the code to test insuranceInsuranceIdDelete
        //instance.insuranceInsuranceIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('insuranceInsuranceIdPharmacyIdDelete', function() {
      it('should call insuranceInsuranceIdPharmacyIdDelete successfully', function(done) {
        //uncomment below and update the code to test insuranceInsuranceIdPharmacyIdDelete
        //instance.insuranceInsuranceIdPharmacyIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('insuranceInsuranceIdPharmacyIdPost', function() {
      it('should call insuranceInsuranceIdPharmacyIdPost successfully', function(done) {
        //uncomment below and update the code to test insuranceInsuranceIdPharmacyIdPost
        //instance.insuranceInsuranceIdPharmacyIdPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('insuranceInsuranceIdPut', function() {
      it('should call insuranceInsuranceIdPut successfully', function(done) {
        //uncomment below and update the code to test insuranceInsuranceIdPut
        //instance.insuranceInsuranceIdPut(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('insurancePost', function() {
      it('should call insurancePost successfully', function(done) {
        //uncomment below and update the code to test insurancePost
        //instance.insurancePost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('inventoryInventoryItemIdPut', function() {
      it('should call inventoryInventoryItemIdPut successfully', function(done) {
        //uncomment below and update the code to test inventoryInventoryItemIdPut
        //instance.inventoryInventoryItemIdPut(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('inventoryPharmacyIdGet', function() {
      it('should call inventoryPharmacyIdGet successfully', function(done) {
        //uncomment below and update the code to test inventoryPharmacyIdGet
        //instance.inventoryPharmacyIdGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('inventoryPharmacyIdProductIdDelete', function() {
      it('should call inventoryPharmacyIdProductIdDelete successfully', function(done) {
        //uncomment below and update the code to test inventoryPharmacyIdProductIdDelete
        //instance.inventoryPharmacyIdProductIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('inventoryPharmacyIdProductIdPost', function() {
      it('should call inventoryPharmacyIdProductIdPost successfully', function(done) {
        //uncomment below and update the code to test inventoryPharmacyIdProductIdPost
        //instance.inventoryPharmacyIdProductIdPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('locationGet', function() {
      it('should call locationGet successfully', function(done) {
        //uncomment below and update the code to test locationGet
        //instance.locationGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyGet', function() {
      it('should call pharmacyGet successfully', function(done) {
        //uncomment below and update the code to test pharmacyGet
        //instance.pharmacyGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyOwnedGet', function() {
      it('should call pharmacyOwnedGet successfully', function(done) {
        //uncomment below and update the code to test pharmacyOwnedGet
        //instance.pharmacyOwnedGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdDelete', function() {
      it('should call pharmacyPharmacyIdDelete successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdDelete
        //instance.pharmacyPharmacyIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdGet', function() {
      it('should call pharmacyPharmacyIdGet successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdGet
        //instance.pharmacyPharmacyIdGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdMemberGet', function() {
      it('should call pharmacyPharmacyIdMemberGet successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdMemberGet
        //instance.pharmacyPharmacyIdMemberGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdMemberPost', function() {
      it('should call pharmacyPharmacyIdMemberPost successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdMemberPost
        //instance.pharmacyPharmacyIdMemberPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdOpeningTimesGet', function() {
      it('should call pharmacyPharmacyIdOpeningTimesGet successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdOpeningTimesGet
        //instance.pharmacyPharmacyIdOpeningTimesGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete', function() {
      it('should call pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete
        //instance.pharmacyPharmacyIdOpeningTimesOpeningTimeIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdOpeningTimesPost', function() {
      it('should call pharmacyPharmacyIdOpeningTimesPost successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdOpeningTimesPost
        //instance.pharmacyPharmacyIdOpeningTimesPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPharmacyIdPatch', function() {
      it('should call pharmacyPharmacyIdPatch successfully', function(done) {
        //uncomment below and update the code to test pharmacyPharmacyIdPatch
        //instance.pharmacyPharmacyIdPatch(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('pharmacyPost', function() {
      it('should call pharmacyPost successfully', function(done) {
        //uncomment below and update the code to test pharmacyPost
        //instance.pharmacyPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productGet', function() {
      it('should call productGet successfully', function(done) {
        //uncomment below and update the code to test productGet
        //instance.productGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productPost', function() {
      it('should call productPost successfully', function(done) {
        //uncomment below and update the code to test productPost
        //instance.productPost(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productProductIdDelete', function() {
      it('should call productProductIdDelete successfully', function(done) {
        //uncomment below and update the code to test productProductIdDelete
        //instance.productProductIdDelete(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productProductIdInventoryItemsGet', function() {
      it('should call productProductIdInventoryItemsGet successfully', function(done) {
        //uncomment below and update the code to test productProductIdInventoryItemsGet
        //instance.productProductIdInventoryItemsGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productProductIdPharmaciesGet', function() {
      it('should call productProductIdPharmaciesGet successfully', function(done) {
        //uncomment below and update the code to test productProductIdPharmaciesGet
        //instance.productProductIdPharmaciesGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('productProductIdPut', function() {
      it('should call productProductIdPut successfully', function(done) {
        //uncomment below and update the code to test productProductIdPut
        //instance.productProductIdPut(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
